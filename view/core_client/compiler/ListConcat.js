//
module.exports = function(){
    this.itens = new Array();
    
    this.add = function(pack){
        this.itens.push(pack);
    };
    
    this.getList = function(){
        return this.itens;
    };
    
    this.getDebugList = function(){
        var a = new Array();
        for(var i in this.itens){
            pack = this.itens[i];
            a.push({ src: pack.origens, dest: pack.getDebugFile()} )
        }
        return a;
    };
    
     this.getDistList = function(){
        var a = new Array();
        for(var i in this.itens){
            pack = this.itens[i];
            a.push({ src: pack.origens, dest: pack.getDistFile()});
        }
        return a;
    }; 
    
       
    this.addComponente = function(componente){
        var tmpComp = new PackJS('componentes/'+componente, [ 'js/componentes/'+componente+'/*']);
        this.add(tmpComp);
    };
    
    
};
