<?php

/*
 * https://qastack.com.br/programming/19778620/provide-an-image-for-whatsapp-link-sharing
 
 */

namespace core\view;

/**
 * Description of OpenGraph
 *
 * @author marci
 */
class OpenGraph
{
    private $view;
    private $properties = [];
    private $obrigatorio = ['image', 'title', 'description', 'url'];
    
    public function __construct(\AbstractView $v)
    {
        $this->view = $v;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setImage($image)
    {
        $this->properties['image'] = BASE_URL . $image;
    }

    public function setUrl($url)
    {
        $this->properties['url'] = $url;
    }

    public function property($property, $value){
        $this->properties[$property] = $value;
    }    
    
    public function process(){
        $this->verificaObrigatorios();
        $meta = '';
        foreach($this->properties as $key => $value){
            $meta .= '<meta property="og:' . $key . '" content="' . $value . '" >';
        }
        return $meta;
    }
    
    private function verificaObrigatorios(){
        foreach($this->obrigatorio as $key => $value){
            if(!isset($this->properties['$key'])){
                if($key == 'url'){
                    //$this->setUrl(BASE_URL);
                } else if($key == 'description') {
                    $this->setDescription($this->view->getDescription());
                } else if($key == 'image'){
                    $this->setImage('/imagens/favicon.png');
                }                  
            }
        } 
    }
   
}
