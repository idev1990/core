<?php

//Constante que define o caminho onde fica o framework servidor do Enyalius
require_once __DIR__. '/../../../confs/config.php';

require_once CORE . 'autoload.php';
require_once CORE . '/libs/tests/CSVFileIterator.php';

define('TESTS_APP_DIR', CORE . '../testes/');