<?php

namespace core\libs\login;

/**
 * Description of LoginMoodle
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class LoginMoodle extends Login
{

    private $moodleURL = 'http://moodle.org';

    public function __construct($chave, $moodleUrl)
    {
        $this->moodleURL = $moodleUrl;
        parent::__construct($chave);
    }

    private function geraSessao($token, User $user)
    {
        $user->addExtra('moodleServer', $this->moodleURL);
        $_SESSION['moodleServer'] = $this->moodleURL;

        $userMoodle = \MoodleUtil::requisita('core_user_get_users_by_field', 
                [
                    'field'=> 'username',
                    'values[0]' => $user->getLogin()
                ], $token)[0];

        $user->addExtra('nome', $userMoodle->fullname);
        
        $url = str_replace('pluginfile.php', 'webservice/pluginfile.php', $userMoodle->profileimageurl);
        $user->addExtra('foto', $url . '&token=' . $token);
           
        $user->addExtra('moodleToken', $token);
        $user->addExtra('moodleID', $userMoodle->id);
        $user->addExtra('email', $userMoodle->email);
        
        $user->setAutenticador('LoginMoodle');
        
        $user->serialize();
    }

    public function verificaLoginSenha($login, $senha, $revalidate = false)
    {
        if ($revalidate) {
            \MoodleUtil::$site = $this->moodleURL;
            $return = \MoodleUtil::geraToken($login, $senha);
            if (!is_null($return) && !isset($return->error)) {
                $this->geraSessao($return->token, $this->geraObjSessao($login, $senha));
                return true;
            }
            if($return->errorcode !== 'invalidlogin'){
                throw new \ErrorException('Erro ' . $return->errorcode);
            }
            #TODO ver os erros possíveis para retornar algo mais interesante ao usuario
            return false;
        } else {
            $user = User::unserialize();
            if ($user->getLogin() == $login) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function verificaLogado()
    {
        $user = User::unserialize();
        if ($user !== null && $user->getAutenticador() == 'LoginMoodle') {
            return true;
        }
        return false;
    }

}
