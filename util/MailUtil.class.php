<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * Classe com métodos que facilitam a manipulação de e-mail
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 2.0
 * @package core.util
 */
class MailUtil
{

    private static $debug = false;
    private static $name = false;
    public static $smtpSecureMode = 'tls';

    public static function send($from, $dest, $title, $message, $mail = false)
    {
        if ($mail) {
            return self::mailFunction($from, $dest, $title, $message);
        }
        return self::sendMail($from, $dest, $title, $message);
    }

    public static function addName($name)
    {
        self::$name = $name;
    }

    public static function debugOn()
    {
        self::$debug = true;
    }

    /**
     * @deprecated since version 2 use send
     * @param type $from
     * @param type $dest
     * @param type $title
     * @param type $message
     * @return boolean
     */
    public static function sendMail($from, $dest, $title, $message)
    {

        $mail = new PHPMailer(TRUE);
        $mail->SetLanguage('br', CORE . 'vendor/phpmailer/phpmailer/language');
        if (self::$debug) {
            $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
        }
        $mail->IsSMTP();

        $mail->SMTPSecure = self::$smtpSecureMode;
        $mail->CharSet = 'UTF-8';
        $mail->Host = MAIL_SERVER;
        $mail->Port = MAIL_PORT;
        $mail->SMTPAuth = true;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;

        $mail->From = $from;
        $mail->FromName = $from;
        $eMails = is_array($dest) ? $dest : explode(',', $dest);
        foreach ($eMails as $destinatario) {
            $mail->AddAddress($destinatario);
        }
        $mail->IsHTML(true);
        $mail->Subject = $title;
        $mail->Body = nl2br($message);

        if (!$mail->Send()) {
            if (self::$debug) {
                $debug = "A mensagem não pode ser enviada" . PHP_EOL . PHP_EOL;

                $debug .= PHP_EOL . 'HEADERS: ' . $headers . PHP_EOL;
                $debug .= PHP_EOL . 'TITLE: ' . $title;
                $debug .= PHP_EOL . 'DESTINATARIO: ' . $dest;

                $debug .= PHP_EOL . 'MESSAGE: ' . $message;



                echo '<pre>' . $debug;
            }
            return false;
        } else {
            return true;
        }
    }

    private static function mailFunction($from, $dest, $title, $message)
    {

        $headers = 'MIME-Version: 1.1' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        // Create email headers
        $headers .= 'From: ' . $from . "\r\n" .
                'Reply-To: ' . $from . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

        $envio = mail($dest, $title, $message, $headers, "-r" . $from);

        if (self::$debug) {
            if ($envio) {
                $debug = "Mensagem enviada com sucesso" . PHP_EOL . PHP_EOL;
            } else {
                $debug = "A mensagem não pode ser enviada" . PHP_EOL . PHP_EOL;
            }

            $debug .= PHP_EOL . 'HEADERS: ' . $headers . PHP_EOL;
            $debug .= PHP_EOL . 'TITLE: ' . $title;
            $debug .= PHP_EOL . 'DESTINATARIO: ' . $dest;

            $debug .= PHP_EOL . 'MESSAGE: ' . $message;



            echo '<pre>' . $debug;
        }

        return $envio;
    }

}
