<?php
$phpVersion = (float)phpversion();


if ($phpVersion >= 5.6) {
    require_once __DIR__. '/HELPER5_6.php';
} else {
    function ds($args) {
        foreach ($args as $a) {
            DebugUtil::show($a);
        }
        var_dump(debug_backtrace());
    }

    /**
     * 
     */
    function dd($args) {
        foreach ($args as $a) {
            DebugUtil::show($a);
        }
        var_dump(debug_backtrace());
        exit();
    }
}