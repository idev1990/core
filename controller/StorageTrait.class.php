<?php

namespace core\controller;

/**
 *  Trait que implementa a funcionalidade de adicionar/gerenciar informações na
 *  sessão de forma transparente ao usuário tanto para tipos primitivos como para objetos
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @author Larissa Rosa
 * @version 1.0
 */
trait StorageTrait
{

    /**
     * Adiciona um dado na sessão 
     * 
     * @param String $key
     * @param misc $value
     */
    public function add($key, $value)
    {
        if (is_object($value)) {
            $_SESSION['storage'][$key]['object'] = serialize($value);
        } else {
            $_SESSION['storage'][$key] = $value;
        }
    }

    /**
     * Método que retorna uma variável salva na sessão.
     * 
     * Retorna nulo em caso de não existencia
     * 
     * @param String $key
     * @return misc
     */
    public function get($key)
    {
        if (isset($_SESSION['storage'][$key])) {
            if (isset($_SESSION['storage'][$key]['object'])) {
                return unserialize($_SESSION['storage'][$key]['object']);
            }
            return $_SESSION['storage'][$key];
        }
        return null;
    }

    public function verify($key)
    {
        return isset($_SESSION['storage'][$key]);
    }

    /**
     * Deleta o campo da sessão
     * 
     * @param String $key
     */
    public function del($key)
    {
        unset($_SESSION['storage'][$key]);
    }

}
