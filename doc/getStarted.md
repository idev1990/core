## Exemplo de CRUD de um usuário

Após a instalação do enyalius e com  um projeto pronto vamos entender a estrutura de pasta e a como funciona um CRUD.

* [model](doc/Model.md)
* [view](doc/View.md)
* [controller](doc/Controler.md)

Atalhos para coisas mais perguntadas:
* [Como internacionalizar - Versões em mais de uma língua](doc/i18n.md)
* [Trabalhando com arquivos](doc/model/oid.md)
