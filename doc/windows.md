## Windows

O tutorial utiliza como base o Docker Toolbox que funciona no windows 7 e em todas as versões do windows 10. Para baixar o docker toolbox utilize:

* https://docs.docker.com/toolbox/toolbox_install_windows/

Ao inciar o docker toolbox é necessário instalar o enyalius na pasta dos binários para isso digite as sequencias de comandos.

```
    cd ~
    mkdir bin
    docker run --rm  -v $PWD:/data mwendler/wget --no-check-certificate https://gitlab.com/enyalius/core/raw/master/enyalius -O /data/eny
```