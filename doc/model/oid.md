## OID ##

O sistema de OID do postgreSQL permite armazenar LargeObjects ou arquivos grandes em banco de dados. 

Vantagens 
* Velocidade em relação ao bytea

Desvantagem
* Necessidade de ter uma coluna para saber qual o Mime do arquivo
* Para garantir a segurança complexidade de consultas

### Arquivos no Enyalius ###
O enyalius da suporte para o OID tanto no gerador de CRUD mas para um bom funcionameno observe as dicas abaixo.

Na tabela:
```
CREATE TABLE teste(
    arquivo oid, -- ponteiro para o arquivo
    arquivo_mime character varyng -- informação para o viewer do enyalius
)
```
