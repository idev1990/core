<?php
/**
 * Description of DTOInterface
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package core.model
 */
interface DTOInterface //extends JsonSerializable
{

    public function getID();
    
    /**
     * @deprecated since version 4.0
     */
    public function getTable();//Talves deixe de existir na versão 4
    
    /**
     * @deprecated since version 4.0
     */
    public function getCondition(); 
    
    /**
     * Método que retorna um array para o objeto Table.
     * 
     * @return Array dados que serão exibidos na tabela 
     */
    public function getArrayJson();
    
    
    
    
}
