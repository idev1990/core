<?php

namespace core\model;

/**
 * A DTO Trait é uma trait que tem por função dar manipular de forma mais adequada
 * e de forma mais transparente todos atributos de um objeto DTO.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package core.model
 */
trait DTOTrait
{

    /**
     * Adiciona em tempo de execução um campo para ser ignorado. 
     * 
     * @param String $field nome do campo a ser ignorado
     */
    public function ignoreField($fields)
    {
        if (!isset($this->ignoreFields)) {
            $this->ignoreFields = array();
        }
        $args = func_get_args();
        if (is_array($fields)) {
            $this->ignoreFields = array_merge($this->ignoreFields, $fields);
        } else if (sizeof($args) > 1) {
            foreach ($args as $field) {
                $this->ignoreFields[] = $field;
            }
        } else {
            $this->ignoreFields[] = $fields;
        }
    }
    
    /** 
     * Campos que deverão ser removidos do vetor de campos ignorados
     * 
     * @param misc $values
     */
    public function considereField($values){
        if (!isset($this->ignoreFields)) {
            return ;
        }
        $args = func_get_args();
        $fields = [];
        foreach ($args as $field) {
            if (is_array($field)){
                $fields = array_merge($fields, $field);
            }else{
                $fields[] = $field;
            }
        }

        $this->ignoreFields = array_diff($this->ignoreFields, $fields);

    }

    /**
     * Retorna um array com o formato 
     *     "campo_tabela" => "valor" 
     * 
     * para inserir no banco
     * 
     * @return Array - Array de dados para inserir 
     */
    public function getArrayDados()
    {
        $campos = array();
        foreach ($this as $chave => $valor) {
            if ($this->verificaCampo($chave, $valor)) {
                //Se for false o banco não faz conversão automatica de vazio para false por isso é necessário enviar um 0
                $campos[\StringUtil::underscoreNumber($chave)] = $valor === false ? '0' : $valor;
            }
        }
        return $campos;
    }

    /**
     * Retorna um array com o formato 
     *     "campo_tabela" => "valor" 
     * 
     * para atualizar tupla em uma tabela no banco
     * 
     * @return Array - Array de dados para atualizar 
     */
    public function getArrayAtualizar()
    {
        $campos = array();
        foreach ($this as $chave => $valor) {
            if ($this->verificaCampo($chave, $valor) && $valor !== null) {
                $campos[\StringUtil::toUnderscore($chave)] = $valor === false ? '0' : $valor;
            }
        }
        return $campos;
    }

    /**
     * Popula o objeto recebendo um array no formato
     * "nomeCampo" => "valor" 
     *
     * @param Array $array - Array de 
     * @return Integer - número de erros encontrados
     */
    public function setArrayDados($array)
    {
        $erros = 0;
        foreach ($array as $campo => $valor) {
            $metodo = 'set' . ucfirst($campo);
            if (method_exists($this, $metodo) && !$this->{$metodo}($this->trataValor($valor))) {
                $erros++;
                $this->isValid = false;
            }
        }
        return $erros;
    }

    /**
     * 
     * @param type $id
     * @return this
     */
    public static function getOne($id)
    {
        $class = __CLASS__;
        if(strrpos($class, '\\') !== false){
            $class = substr(__CLASS__, strrpos(__CLASS__, '\\')+1);
        }
        $daoName = $class . 'DAO';
        $dao = new $daoName();
        return $dao->getById($id);
    }
    
     /**
     * Método que atualiza ou salva o objeto 
     */
    public function save()
    {
        $class = substr(__CLASS__, strrpos(__CLASS__, '\\')); 
        $daoName = $class . 'DAO';
        $dao = new $daoName();
        return $dao->save($this);
    }

    public function load($id){
        $obj = self::getOne($this->getID()); 
        foreach ($this as $chave => $valor) {
            if ($this->verificaCampo($chave, $valor) && $valor !== null) {
                $campos[\StringUtil::toUnderscore($chave)] = $valor === false ? '0' : $valor; 
            }
        }
    }
    
    public static function getAll($condition = false)
    {
        $class = substr(__CLASS__, strrpos(__CLASS__, '\\')+1); 
        $daoName = $class . 'DAO';
        $dao = new $daoName();
        return $dao->getLista($condition);
    }

    /**
     * Recebe valores através de variável valor e caso necessário
     * faz o parser dos objetos como o caso de objetos geográficos ou arquivos.
     * 
     * @param misc $valor
     * @return misc pode ser o próprio valor ou um objeto.
     */
    private function trataValor($valor)
    {
        if (is_string($valor) && strpos($valor, 'POINT(') !== false) {
            return new \Point($valor);
        }
        return $valor;
    }

    /**
     * Verifica se o parametro do objeto vai ser usado para o 
     * mapeamento objeto relacional. 
     *       
     * @param string $chave
     * @param misc $valor
     * @return boolean
     */
    private function verificaCampo($chave, $valor)
    {
        $ignore = isset($this->ignoreFields) ? !(in_array($chave, $this->ignoreFields)) : true;
        $id = ( strcasecmp($chave, 'id' . __CLASS__) != 0 && $chave != 'id' );
        return ( $id && ($chave != 'isValid') && ($chave != 'table') && ($chave != 'ignoreFields') && $ignore && ($chave[0] != '_') && !is_array($valor));
    }

}
